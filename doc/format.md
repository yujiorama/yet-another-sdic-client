辞書形式まとめ
====

sdic.texi から抜粋。

## COMPAT

> COMPAT形式は、look を使った前方一致検索が高速に行える構造になっている。
> また、構造が単純なので、辞書のファイルサイズが小さく抑えられる。

    file     := line*
    line     := headword '\t' content '\n'
    headword := string
    content  := string
    string   := char+
    char     := [^\t\n]

## SDIC

> SDIC形式は、grep などの行指向の検索プログラムを使って前方一致検索及び後
> 方一致検索、完全一致検索、全文検索が容易に行なえる構造になっている。

    file     := line*
    line     := comment | entry
    comment  := '#' [^\n]* '\n'
    entry    := headword keyword* content '\n'
    headword := '<H>' word '</H>' | keyword
    keyword  := '<K>' word '</K>'
    word     := char+
    char     := [^<>&\n] | '&lt;' | '&gt;' | '&amp;'
    content  := string
    string   := char_lf+
    char_lf  := char | '&lf;'

* `headword`
    - 見出し語
    - 表示用と考えて良い
* `keyword`
    - 検索語
    - アルファベットはすべて小文字
    - カタカナはすべてひらがな
    - 複数の空白文字は1つに

import sublime
import sublime_plugin
import sqlite3
import contextlib
import os.path

@contextlib.contextmanager
def open_connection(name):
    conn = sqlite3.connect(name)
    try:
        yield conn
        conn.commit()
    except sqlite3.Error as exc:
        conn.rollback()
    finally:
        if conn:
            conn.close()

@contextlib.contextmanager
def open_cursor(conn, autocommit=False):
    cursor = conn.cursor()
    try:
        yield cursor
        if autocommit:
            conn.commit()
    except sqlite3.Error as exc:
        if autocommit:
            conn.rollback()
    finally:
        if cursor:
            cursor.close()

def plugin_loaded():
    pass

class Yasdic(sublime_plugin.EventListener):
    def on_query_completions(self, view, prefix, locations):
        global settings
        settings = sublime.load_settings("yasdic.sublime-settings")
        plugin_enabled = settings.get("enable", False)
        if not plugin_enabled:
            print("plugin not enabled")
            return None
        db = settings.get("dictionary", "")
        if not db:
            print("dictionary not specified")
            return None
        if not os.path.exists(db):
            print("dictionary not found: " + db)
            return None
        max_candidate = settings.get("max_candidate", 10)
        with open_connection(os.path.abspath(db)) as conn:
            with open_cursor(conn, True) as cursor:
                cursor.execute("select keyword, data, source from dict where keyword like ? order by keyword limit ?", (prefix + '%', max_candidate))
                xs = [(row[0] + "\t" + row[1], row[1]) for row in cursor.fetchall()]
                return (xs, sublime.INHIBIT_WORD_COMPLETIONS | sublime.INHIBIT_EXPLICIT_COMPLETIONS)

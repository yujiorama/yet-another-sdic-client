# -*- coding: utf-8 -*-

import contextlib
from logging import getLogger, StreamHandler, DEBUG
logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)
logger.propagate = False
import os.path
import re
import sqlite3
import sys
import xml.sax
import xml.sax.handler

@contextlib.contextmanager
def open_connection(name):
    conn = sqlite3.connect(name)
    try:
        yield conn
        conn.commit()
    except sqlite3.Error as exc:
        conn.rollback()
        logger.exception('error :%s', exc)
    finally:
        if conn:
            conn.close()

@contextlib.contextmanager
def open_cursor(conn, autocommit=False):
    cursor = conn.cursor()
    try:
        yield cursor
        if autocommit:
            conn.commit()
    except sqlite3.Error as exc:
        if autocommit:
            conn.rollback()
        logger.exception('error :%s', exc)
    finally:
        if cursor:
            cursor.close()

class SdicRecordHandler(xml.sax.handler.ContentHandler):
    """sdic record handler
    """
    def __init__(self, conn, source, interval=1000):
        self.conn = conn
        self.source = source
        self.count = 0
        self.interval = interval
        self.node = []
        self.heading = None
        self.keywords = []
        self.data = None
        super(SdicRecordHandler, self).__init__()

    def startElement(self, name, attrs):
        self.node.append(name)

    def endElement(self, name):
        node = self.node.pop()
        if node == 'root':
            heading = (self.heading or self.keywords[0])
            data = self.data
            source = self.source
            keyword = '\n'.join(self.keywords)
            values = (heading, data, source, keyword)
            with open_cursor(self.conn, False) as cursor:
                cursor.execute("insert into dict(heading, data, source, keyword) values (?, ?, ?, ?)", values)
                self.count += 1
            if self.count > self.interval:
                self.conn.commit()
                self.count = 0
            self.heading = None
            self.keywords = []
            self.data = None

    def characters(self, data):
        if self.node[-1] == 'H':
            self.heading = data
        elif self.node[-1] == 'K':
            self.keywords.append(data)
        else:
            self.data = data


class IgnoreErrorHandler(xml.sax.handler.ErrorHandler):
    """エラーを無視するハンドラ
    """
    def __init__(self):
        super(IgnoreErrorHandler, self).__init__()

    def error(self, exception):
        pass

    def fatalError(self, exception):
        pass

    def warning(self, exception):
        pass


def sdic2sqlite3(source):
    """sdic 辞書ファイルを sqlite3 データベースへ変換します。
    """
    with open_connection('yasdic.db') as conn:
        with open_cursor(conn, True) as cursor:
            cursor.execute('''CREATE TABLE IF NOT EXISTS dict(
                heading TEXT,
                data TEXT,
                source TEXT,
                keyword TEXT
                )''')
            cursor.execute('delete from dict where source in (?)', [source])
        handler = SdicRecordHandler(conn, source)
        error_handler = IgnoreErrorHandler()
        lines = 0
        for line in sys.stdin:
            if re.match(r"^\s*#.*", line):
                continue
            doc = "<root>{}</root>".format(line.strip())
            xml.sax.parseString(doc, handler, error_handler)
            lines += 1

        with open_cursor(conn, True) as cursor:
            cursor.execute('select count(*) from dict where source in (?)', [source])
            row = cursor.fetchone()
            print('sdic={},db={}'.format(lines, row[0]))


if __name__ == '__main__':
    source = sys.argv.pop()
    if source == os.path(__file__).basename:
        sys.exit(1)

    sdic2sqlite3(source)

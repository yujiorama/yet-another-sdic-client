yet another sdic client
====

[sdic](http://www.namazu.org/~tsuchiya/sdic/) の使用する辞書ファイルを加工して利用する Sublime Text のプラグイン。
英単語の和訳候補がサジェスチョンに登場する。

![screenshot](./screenshot1.png)

## Link

* [sdic](http://www.namazu.org/~tsuchiya/sdic/)
* [GENE95辞書](http://www.namazu.org/~tsuchiya/sdic/data/gene.html)
* [The EDICT Dictionary File](http://www.edrdg.org/jmdict/edict.html)
* [Algorithms with Python 接尾辞配列 (suffix array)](http://www.geocities.jp/m_hiroi/light/pyalgo43.html)
* [Suffix Array を使った文字列マッチング](https://blog.shibayu36.org/entry/2017/01/13/073000)
* [SQLite FTS3 and FTS4 Extensions](https://www.sqlite.org/fts3.html)

## How to Install

* Package Control の `Add Repository` を選択
* `https://bitbucket.org/yujiorama/yet-another-sdic-cliet` と入力
* Package Control の `Install Package` を選択
* `yet-another-sdic-cliet` を選択
* Sublime Text を再起動
* Preferences > Package Settings > yasdic > Settings を開いて設定値を指定

| 設定値        | 初期値 | 意味                                       |
|:-------------:|:------:|:-------------------------------------------|
| max_candidate | 10     | 登場する候補数                             |
| dictionary    | なし   | 辞書ファイルの完全パス(作成方法は後述)     |
| enable        | true   | プラグイン動作の制御                       |

## Create Dictionary file

### 形式

sqlite3 形式のデータベースファイルを利用します。

テーブルレイアウトは次のとおり。

```SQL
CREATE TABLE IF NOT EXISTS
dict(
  heading TEXT,
  data TEXT,
  source TEXT,
  keyword TEXT
)
```

### 作成方法

`sdic2sqlite3.py` スクリプトで辞書ファイルの作成とデータ登録を行います。

先に sdic の手順に従って `gene.sdic` および `eedict.sdic` を作成しておきます。

```bash
$ python sdic2sqlite3.py gene < gene.sdic
$ python sdic2sqlite3.py edict < eedict.sdic
$ ls -l yasdic.db
```

### 利用方法

作成したデータベースファイル `yasdic.db` の完全パスをプラグインの設定ファイルに指定してください。

## LICENSE

MIT License
